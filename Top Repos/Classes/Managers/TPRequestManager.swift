//
//  TPRequestManager.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class TPRequestManager: NSObject
{
    //
    // MARK: - Properties
    
    var headers: HTTPHeaders = [:];
    
    //
    // MARK: - Public methods
    
    func get(url: String, parameters: [String : Any]?, success: @escaping (Any?) -> Void, failure: @escaping (TPErrorResponse) -> Void, encoding: ParameterEncoding = JSONEncoding.default)
    {
        self.setupHeaders();
        
        self.requestWithUrl(url: url, method: .get, parameters: parameters, encoding: encoding).responseJSON(completionHandler: { (response) in
            
            let isValid = self.validateResponse(response);
            
            if (isValid)
            {
                success(response.result.value);
            }
            else
            {
                failure(self.handleErrorWithResponse(response));
            }
        });
    }
    
    //
    // MARK: - Private methods
    
    private func handleErrorWithResponse(_ response: DataResponse<Any>) -> TPErrorResponse
    {
        if let error = Mapper<TPErrorResponse>().map(JSONObject: response.result.value)
        {
            return error;
        }
        else
        {
            return TPErrorResponse(message: "Ocorreu um erro inesperado.");
        }
    }
    
    private func requestWithUrl(url: String, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding = JSONEncoding.default) -> DataRequest
    {
        let dataRequest = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: self.headers).validate(contentType: ["application/json"]);
        
        return dataRequest;
    }
    
    private func setupHeaders()
    {
        self.headers["Content-Type"] = "application/json; charset=utf-8";
    }
    
    private func validateResponse(_ response: DataResponse<Any>) -> Bool
    {
        if let requestResponse = response.response
        {
            if (requestResponse.statusCode >= 200 && requestResponse.statusCode < 300)
            {
                return true;
            }
            
            print(String(format: "Request Error Code: %d", requestResponse.statusCode));
        }
        
        return false;
    }
}
