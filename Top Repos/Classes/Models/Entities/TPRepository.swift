//
//  TPRepository.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPRepository: TPBaseModel
{
    //
    // MARK: - Properties
    
    var forks: NSNumber!;
    var name:  String!;
    var owner: TPOwner!;
    var stars: NSNumber!;
    
    //
    // MARK: - Init methods
    
    required init?(map: Map)
    {
        super.init(map: map);
    }
    
    override init()
    {
        super.init();
    }
    
    //
    // MARK: - Public methods
    
    override func mapping(map: Map)
    {
        super.mapping(map: map);
        
        self.forks <- map["forks_count"];
        self.name  <- map["name"];
        self.owner <- map["owner"];
        self.stars <- map["stargazers_count"];
    }
}

extension TPRepository
{
    func getAuthorFormatted() -> String?
    {
        if let author = self.owner?.name
        {
            return String(format: "Author: %@", author);
        }
        
        return nil;
    }
    
    func getForksFormatted() -> String?
    {
        let numberFormatter = NumberFormatter();
        
        numberFormatter.groupingSeparator = ",";
        numberFormatter.numberStyle = .decimal;
        
        return numberFormatter.string(from: self.forks);
    }
    
    func getStarsFormatted() -> String?
    {
        let numberFormatter = NumberFormatter();
        
        numberFormatter.groupingSeparator = ",";
        numberFormatter.numberStyle = .decimal;
        
        return numberFormatter.string(from: self.stars);
    }
}
