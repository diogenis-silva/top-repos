//
//  TPOwner.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPOwner: TPBaseModel
{
    //
    // MARK: - Properties
    
    var name:  String!;
    var photo: String!;
    
    //
    // MARK: - Init methods
    
    required init?(map: Map)
    {
        super.init(map: map);
    }
    
    //
    // MARK: - Public methods
    
    override func mapping(map: Map)
    {
        super.mapping(map: map);
        
        self.name  <- map["login"];
        self.photo <- map["avatar_url"];
    }
}
