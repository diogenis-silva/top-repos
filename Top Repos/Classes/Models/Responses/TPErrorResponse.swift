//
//  TPErrorResponse.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPErrorResponse: TPBaseModel
{
    //
    // MARK: - Properties
    
    var message: String!
    
    //
    // MARK: - Init methods
    
    required init?(map: Map)
    {
        super.init(map: map);
    }
    
    init(message: String)
    {
        super.init();
        
        self.message = message;
    }
    
    //
    // MARK: - Public methods
    
    override func mapping(map: Map)
    {
        super.mapping(map: map);
        
        self.message <- map["message"];
    }
}
