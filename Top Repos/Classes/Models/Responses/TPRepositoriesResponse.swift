//
//  TPRepositoriesResponse.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPRepositoriesResponse: TPBaseModel
{
    //
    // MARK: - Properties
    
    var repositories: Array<TPRepository>!;
    var total: NSNumber!;
    
    //
    // MARK: - Init methods
    
    required init?(map: Map)
    {
        super.init(map: map);
    }
    
    //
    // MARK: - Public methods
    
    override func mapping(map: Map)
    {
        super.mapping(map: map);
        
        self.repositories <- map["items"];
        self.total        <- map["total_count"];
    }
}
