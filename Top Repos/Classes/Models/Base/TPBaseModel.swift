//
//  TPBaseModel.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPBaseModel: NSObject, Mappable
{
    //
    // MARK: - Init methods
    
    override init()
    {
        super.init();
    }
    
    required init?(map: Map)
    {
        
    }
    
    //
    // MARK: - Public methods
    
    func mapping(map: Map)
    {
        
    }
}
