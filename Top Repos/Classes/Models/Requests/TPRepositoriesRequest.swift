//
//  TPRepositoriesRequest.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ObjectMapper

class TPRepositoriesRequest: TPBaseModel
{
    //
    // MARK: - Properties
    
    var page:  NSNumber!;
    var query: String!;
    var sort:  String!;
    
    //
    // MARK: - Init methods
    
    required init?(map: Map)
    {
        super.init(map: map);
    }
    
    override init()
    {
        super.init();
    }
    
    //
    // MARK: - Public methods
    
    override func mapping(map: Map)
    {
        super.mapping(map: map);
        
        self.page  <- map["page"];
        self.query <- map["q"];
        self.sort  <- map["sort"];
    }
}
