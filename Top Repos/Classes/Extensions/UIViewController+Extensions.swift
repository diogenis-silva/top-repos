//
//  UIViewController+Extensions.swift
//  Top Repos
//
//  Created by Diogenis Silva on 14/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit

extension UIViewController
{
    func showAlertWithTitle(_ title: String = "Aviso", message: String)
    {
        let alertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in return };
        alertAction.accessibilityLabel = "alertControllerOk";
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert);
        alertController.addAction(alertAction);
        
        self.present(alertController, animated: true, completion: nil);
    }
}
