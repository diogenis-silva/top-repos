//
//  TPViewCodeDelegate.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit

protocol TPViewCodeDelegate: class
{
    func initViews();
    func setupAppearance();
    func setupConstraints();
    func setupView();
}

extension TPViewCodeDelegate
{
    func setupView()
    {
        self.initViews();
        self.setupAppearance();
        self.setupConstraints();
    }
}
