//
//  TPGitAPI.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class TPGitAPI: TPRequestManager
{
    func fetchRepositoriesWithParameters(_ parameters: TPRepositoriesRequest, success: @escaping (TPRepositoriesResponse) -> Void, failure: @escaping (TPErrorResponse) -> Void)
    {
        let url = "https://api.github.com/search/repositories";
        
        self.get(url: url, parameters: parameters.toJSON(), success: { (response) in
            
            if let result = Mapper<TPRepositoriesResponse>().map(JSONObject: response)
            {
                success(result);
            }
            else
            {
                failure(TPErrorResponse(message: "Não foi possível carregar os repositórios no momento."));
            }
            
        }, failure: { (error) in
            
            failure(error);
            
        }, encoding: URLEncoding.default);
    }
}
