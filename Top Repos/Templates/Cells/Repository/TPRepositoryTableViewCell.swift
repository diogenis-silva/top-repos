//
//  TPRepositoryTableViewCell.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import FontAwesomeKit
import SnapKit
import SDWebImage

class TPRepositoryTableViewCell: UITableViewCell, TPViewCodeDelegate
{
    //
    // MARK: - Properties
    
    var repository: TPRepository! {
        didSet { self.loadData() }
    };
    
    //
    // MARK: - Views
    
    var imageViewPhoto:   UIImageView!
    var labelAuthor:      UILabel!;
    var labelName:        UILabel!;
    var viewCounterForks: TPCounterView!;
    var viewCounterStars: TPCounterView!;
    
    //
    // MARK : - View methods
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        self.setupCell();
        self.setupView();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented");
    }
    
    //
    // MARK: - Private methods
    
    private func loadData()
    {
        // self.imageViewPhoto.sd_setImage(with: URL(string: self.repository.owner.photo));
        
        self.imageViewPhoto.sd_setImage(with: URL(string: self.repository.owner.photo));
        
        self.labelAuthor.text = self.repository.getAuthorFormatted();
        self.labelName.text = self.repository.name.uppercased();
        
        self.viewCounterStars.icon = FAKFontAwesome.starIcon(withSize: 11);
        self.viewCounterStars.title = "Star";
        self.viewCounterStars.value = self.repository.getStarsFormatted();
        
        self.viewCounterForks.icon = FAKFontAwesome.codeForkIcon(withSize: 11);
        self.viewCounterForks.title = "Fork";
        self.viewCounterForks.value = self.repository.getForksFormatted();
    }
    
    private func setupCell()
    {
        self.selectionStyle = .none;
    }
    
    //
    // MARK: - View Code delegate methods
    
    func initViews()
    {
        self.imageViewPhoto = UIImageView(frame: .zero);
        self.labelName = UILabel(frame: .zero);
        self.labelAuthor = UILabel(frame: .zero);
        self.viewCounterStars = TPCounterView(frame: .zero);
        self.viewCounterForks = TPCounterView(frame: .zero);
        
        self.contentView.addSubview(self.imageViewPhoto);
        self.contentView.addSubview(self.labelName);
        self.contentView.addSubview(self.labelAuthor);
        self.contentView.addSubview(self.viewCounterStars);
        self.contentView.addSubview(self.viewCounterForks);
    }
    
    func setupAppearance()
    {
        self.imageViewPhoto.backgroundColor = UIColor.clear;
        
        self.labelAuthor.font = UIFont.systemFont(ofSize: 13);
        self.labelAuthor.textColor = UIColor.flatGray();
        
        self.labelName.font = UIFont.boldSystemFont(ofSize: 13);
        self.labelName.textColor = UIColor.flatBlack();
    }
    
    func setupConstraints()
    {
        self.imageViewPhoto.snp.makeConstraints { (constraint) in
            constraint.left.equalToSuperview().offset(10);
            constraint.top.equalToSuperview().offset(15);
            constraint.height.equalTo(50);
            constraint.width.equalTo(50);
        };
        
        self.labelName.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.imageViewPhoto.snp.right).offset(10);
            constraint.right.equalToSuperview().offset(-10);
            constraint.top.equalToSuperview().offset(15);
        };
        
        self.labelAuthor.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.imageViewPhoto.snp.right).offset(10);
            constraint.right.equalToSuperview().offset(-10);
            constraint.top.equalTo(self.labelName.snp.bottom).offset(3);
        };
        
        self.viewCounterStars.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.imageViewPhoto.snp.right).offset(10);
            constraint.top.equalTo(self.labelAuthor.snp.bottom).offset(10);
            constraint.bottom.equalToSuperview().offset(-15);
            constraint.height.equalTo(20);
        };
        
        self.viewCounterForks.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.viewCounterStars.snp.right).offset(10);
            constraint.top.equalTo(self.labelAuthor.snp.bottom).offset(10);
            constraint.bottom.equalToSuperview().offset(-15);
            constraint.height.equalTo(20);
        };
    }
}
