//
//  TPCounterView.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import ChameleonFramework
import FontAwesomeKit

class TPCounterView: UIView, TPViewCodeDelegate
{
    //
    // MARK: - Properties
    
    var icon: FAKFontAwesome? {
        didSet { self.imageViewTitle.image = self.icon?.image(with: CGSize(width: 15, height: 15)); }
    }
    
    var title: String? {
        didSet { self.labelTitle.text = self.title }
    }
    
    var value: String? {
        didSet { self.labelValue.text = self.value }
    }
    
    //
    // MARK: - Views
    
    var imageViewTitle: UIImageView!;
    var labelTitle:     UILabel!;
    var labelValue:     UILabel!;
    var viewSeparator:  UIView!;
    var viewTitle:      UIView!;
    
    //
    // MARK: - View methods
    
    override init(frame: CGRect)
    {
        super.init(frame: frame);
        
        self.setupView();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // MARK: - View Code delegate methods
    
    func initViews()
    {
        self.imageViewTitle = UIImageView(frame: .zero);
        self.labelTitle = UILabel(frame: .zero);
        self.labelValue = UILabel(frame: .zero);
        self.viewSeparator = UIView(frame: .zero);
        self.viewTitle = UIView(frame: .zero);
        
        self.viewTitle.addSubview(self.imageViewTitle);
        self.viewTitle.addSubview(self.labelTitle);
        
        self.addSubview(self.viewTitle);
        self.addSubview(self.viewSeparator);
        self.addSubview(self.labelValue);
    }
    
    func setupAppearance()
    {
        self.backgroundColor = UIColor.white;
        self.clipsToBounds = true;
        self.layer.borderColor = UIColor.flatWhiteColorDark()?.cgColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 3;
        
        self.labelTitle.font = UIFont.boldSystemFont(ofSize: 9);
        self.labelTitle.textAlignment = .center;
        self.labelValue.font = UIFont.boldSystemFont(ofSize: 9);
        
        self.imageViewTitle.backgroundColor = UIColor.clear;
        self.imageViewTitle.contentMode = .scaleAspectFit;
        
        self.viewSeparator.backgroundColor = UIColor.flatWhiteColorDark();
        self.viewTitle.backgroundColor = UIColor.flatWhite();
    }
    
    func setupConstraints()
    {
        self.viewTitle.snp.makeConstraints { (constraint) in
            constraint.left.equalToSuperview().offset(0);
            constraint.top.equalToSuperview().offset(0);
            constraint.bottom.equalToSuperview().offset(0);
        };
        
        self.imageViewTitle.snp.makeConstraints { (constraint) in
            constraint.left.equalToSuperview().offset(5);
            constraint.top.equalToSuperview().offset(0);
            constraint.bottom.equalToSuperview().offset(0);
            constraint.width.equalTo(15);
        };
        
        self.labelTitle.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.imageViewTitle.snp.right).offset(5);
            constraint.right.equalToSuperview().offset(-5);
            constraint.top.equalToSuperview().offset(0);
            constraint.bottom.equalToSuperview().offset(0);
        };
        
        self.viewSeparator.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.viewTitle.snp.right).offset(0);
            constraint.top.equalToSuperview().offset(0);
            constraint.bottom.equalToSuperview().offset(0);
            constraint.width.equalTo(1);
        };
        
        self.labelValue.snp.makeConstraints { (constraint) in
            constraint.left.equalTo(self.viewSeparator.snp.right).offset(5);
            constraint.right.equalToSuperview().offset(-5);
            constraint.top.equalToSuperview().offset(0);
            constraint.bottom.equalToSuperview().offset(0);
        };
    }
}
