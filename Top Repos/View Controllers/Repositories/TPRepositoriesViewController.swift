//
//  TPRepositoriesViewController.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit

class TPRepositoriesViewController: TPBaseViewController, TPRepositoriesViewDelegate
{
    //
    // MARK: - Properties
    
    var currentPage:      NSNumber = 1;
    var viewRepositories: TPRepositoriesView!;
    
    //
    // MARK: - View methods
    
    override func loadView()
    {
        super.loadView();
        
        self.setupViews();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.setupTitle();
        self.fetchRepositories();
    }
    
    //
    // MARK: - Private methods
    
    private func fetchRepositories(isRefresh: Bool = false)
    {
        let parameters = self.getRepositoriesRequestParameters();
        
        self.showHUD();
        
        TPGitAPI().fetchRepositoriesWithParameters(parameters, success: { (response) in
            
            self.hideHUD();
            self.loadRepositoriesWithResponse(response, isRefresh: isRefresh);
            self.finishLoadData();
            
        }, failure: { (error) in
            
            self.hideHUD();
            self.showAlertWithTitle(message: error.message);
            self.finishLoadData();
            
        });
    }
    
    private func finishLoadData()
    {
        self.viewRepositories.finishLoadData();
    }
    
    private func getRepositoriesRequestParameters() -> TPRepositoriesRequest
    {
        let parameters = TPRepositoriesRequest();
        
        parameters.page = self.currentPage;
        parameters.query = "language:swift";
        parameters.sort = "stars";
        
        return parameters;
    }
    
    private func loadRepositoriesWithResponse(_ response: TPRepositoriesResponse, isRefresh: Bool)
    {
        self.viewRepositories.loadDataWithRepositories(response.repositories, isRefresh: isRefresh);
    }
    
    private func setupTitle()
    {
        self.title = "Top Swift Repositories";
    }
    
    private func setupViews()
    {
        self.viewRepositories = TPRepositoriesView(frame: UIScreen.main.bounds);
        self.viewRepositories.delegate = self;
        
        self.view.addSubview(self.viewRepositories);
    }
    
    //
    // MARK: Repositories View delegate methods
    
    func repositoriesViewDidPageRepositories(_ view: TPRepositoriesView)
    {
        self.currentPage = NSNumber(value: self.currentPage.intValue + 1);
        self.fetchRepositories();
    }
    
    func repositoriesViewDidRefreshRepositories(_ view: TPRepositoriesView)
    {
        self.currentPage = NSNumber(value: 1);
        self.fetchRepositories(isRefresh: true);
    }
}
