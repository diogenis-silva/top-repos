//
//  TPRepositoriesView.swift
//  Top Repos
//
//  Created by Diogenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll
import SnapKit

protocol TPRepositoriesViewDelegate: class
{
    func repositoriesViewDidPageRepositories(_ view: TPRepositoriesView);
    func repositoriesViewDidRefreshRepositories(_ view: TPRepositoriesView);
}

class TPRepositoriesView: UIView, TPViewCodeDelegate, UITableViewDataSource, UITableViewDelegate
{
    //
    // MARK: - Properties
    
    var arrayRepositories: Array<TPRepository> = [];
    
    weak var delegate: TPRepositoriesViewDelegate?;
    
    //
    // MARK: - Views
    
    var refreshControl:        UIRefreshControl!;
    var tableViewRepositories: UITableView!;
    
    //
    // MARK: - View methods
    
    override init(frame: CGRect)
    {
        super.init(frame: frame);
        
        self.setupView();
        self.setupRefreshControl();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // MARK: - Public methods
    
    func finishLoadData()
    {
        self.tableViewRepositories.finishInfiniteScroll();
        self.tableViewRepositories.reloadData();
        self.refreshControl.endRefreshing();
        
    }
    
    func loadDataWithRepositories(_ repositories: Array<TPRepository>, isRefresh: Bool)
    {
        if (isRefresh)
        {
            self.arrayRepositories = repositories;
        }
        else
        {
            self.arrayRepositories.append(contentsOf: repositories);
        }
    }
    
    //
    // MARK: - Private methods
    
    private func cellRepositoryWithIndexPath(_ indexPath: IndexPath) -> UITableViewCell
    {
        let uid = "TPRepositoryTableViewCell";
        let cell = self.tableViewRepositories.dequeueReusableCell(withIdentifier: uid) as! TPRepositoryTableViewCell;
        
        cell.repository = self.arrayRepositories[indexPath.row];
        
        return cell;
    }
    
    private func didPageRepositories()
    {
        self.delegate?.repositoriesViewDidPageRepositories(self);
    }
    
    @objc private func didRefreshRepositories()
    {
        self.delegate?.repositoriesViewDidRefreshRepositories(self);
    }
    
    private func setupRefreshControl()
    {
        self.refreshControl = UIRefreshControl();
        self.refreshControl.attributedTitle = NSAttributedString(string: "Arraste para atualizar");
        self.refreshControl.addTarget(self, action: #selector(self.didRefreshRepositories), for: UIControl.Event.valueChanged)
        
        self.tableViewRepositories.addSubview(self.refreshControl);
    }
    
    private func setupTableView()
    {
        self.tableViewRepositories.accessibilityLabel = "tableViewRepositories";
        self.tableViewRepositories.dataSource = self;
        self.tableViewRepositories.delegate = self;
        self.tableViewRepositories.estimatedRowHeight = 90;
        self.tableViewRepositories.tableFooterView = UIView();
        self.tableViewRepositories.rowHeight = UITableView.automaticDimension;
        
        self.tableViewRepositories.register(TPRepositoryTableViewCell.self, forCellReuseIdentifier: "TPRepositoryTableViewCell");
        
        self.tableViewRepositories.addInfiniteScroll(handler: { (tableView) in
            self.didPageRepositories();
        });
    }
    
    //
    // MARK: - View Code delegate methods
    
    func initViews()
    {
        self.tableViewRepositories = UITableView(frame: .zero, style: .plain);
        
        self.addSubview(self.tableViewRepositories);
        
        self.setupTableView();
    }
    
    func setupAppearance()
    {
        self.tableViewRepositories.separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    }
    
    func setupConstraints()
    {
        self.tableViewRepositories.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0);
            make.right.equalToSuperview().offset(0);
            make.top.equalToSuperview().offset(0);
            make.bottom.equalToSuperview().offset(0);
        };
    }
    
    //
    // MARK: - Table View delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayRepositories.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return self.cellRepositoryWithIndexPath(indexPath);
    }
}
