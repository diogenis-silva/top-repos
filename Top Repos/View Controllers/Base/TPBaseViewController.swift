//
//  TPBaseViewController.swift
//  Top Repos
//
//  Created by Diógenis Silva on 12/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import MBProgressHUD

class TPBaseViewController: UIViewController
{
    //
    // MARK: - View methods
    
    override func loadView()
    {
        super.loadView();
        
        self.setupView();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
    }
    
    //
    // MARK: - Public methods
    
    func hideHUD()
    {
        if let rootView = UIApplication.shared.windows.first
        {
            MBProgressHUD.hide(for: rootView, animated: true);
        }
    }
    
    func showHUD(blockView: Bool = false)
    {
        if let rootView = UIApplication.shared.windows.first
        {
            let hud = MBProgressHUD.showAdded(to:rootView, animated: true);
            hud.isUserInteractionEnabled = blockView;
        }
    }
    
    //
    // MARK: - Private methods
    
    private func setupView()
    {
        let view = UIView(frame: UIScreen.main.bounds);
        
        view.backgroundColor = UIColor.white;
        
        self.view = view;
    }
}
