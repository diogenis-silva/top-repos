//
//  AppDelegate.swift
//  Top Repos
//
//  Created by Diógenis Silva on 11/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    //
    // MARK: - Properties
    
    var window: UIWindow?

    //
    // MARK: - App Delegate methods
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        self.setupInitialView();
        
        return true;
    }
    
    //
    // MARK: - Private methods
    
    private func setupInitialView()
    {
        let rootViewController = TPRepositoriesViewController();
        let navigationController = UINavigationController(rootViewController: rootViewController);
        
        self.window = UIWindow(frame: UIScreen.main.bounds);
        self.window?.rootViewController = navigationController;
        self.window?.makeKeyAndVisible();
    }
}

