//
//  TPHelper.swift
//  Top Repos Tests
//
//  Created by Diogenis Silva on 14/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit

class TPHelper
{
    class func JSONFromFile(_ file: String) -> Dictionary<String, Any>?
    {
        if let path = Bundle.main.path(forResource: file, ofType: "json")
        {
            do
            {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe);
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves);
                
                if let result = jsonResult as? Dictionary<String, Any>
                {
                    return result;
                }
            }
            catch
            {
                print(error.localizedDescription);
            }
        }
        
        return nil;
    }
}
