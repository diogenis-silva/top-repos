//
//  TPKIF+Extensions.swift
//  Top Repos Tests
//
//  Created by Diogenis Silva on 14/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import KIF

extension XCTestCase
{
    func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor
    {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
    
    func system(file : String = #file, _ line : Int = #line) -> KIFSystemTestActor
    {
        return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
    }
}

extension KIFTestActor
{
    func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor
    {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
    
    func system(file : String = #file, _ line : Int = #line) -> KIFSystemTestActor
    {
        return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
    }
}
