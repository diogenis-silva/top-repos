//
//  TPRepositoriesViewSpec.swift
//  Top Repos Tests
//
//  Created by Diogenis Silva on 14/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots
import ObjectMapper

@testable import Top_Repos

class TPRepositoriesViewSpec: QuickSpec
{
    //
    // MARK: - Spec methods
    
    override func spec()
    {
        describe("The UI", closure: {
            
            it("should have the expected look and feel", closure: {
                
                let frame = CGRect(x: 0, y: 0, width: 320, height: 568);
                let view = TPRepositoriesView(frame: frame);
                
                view.loadDataWithRepositories(self.getRepositories(), isRefresh: false);
                
                expect(view) == snapshot("TPRepositoriesView");
                
            });
        });
    }
    
    //
    // MARK: - Private methods
    
    private func getRepositories() -> Array<TPRepository>
    {
        if let response = TPHelper.JSONFromFile("mock-repositories")
        {
            if let result = Mapper<TPRepositoriesResponse>().map(JSONObject: response)
            {
                return result.repositories;
            }
        }
        
        return [];
    }
}
