//
//  Top_ReposTests.swift
//  Top ReposTests
//
//  Created by Diógenis Silva on 11/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots
import FontAwesomeKit

@testable import Top_Repos

class TPCounterViewSpec: QuickSpec
{
    //
    // MARK: - Spec methods
    
    override func spec()
    {
        describe("The UI", closure: {
            
            it("should have the expected look and feel", closure: {
                
                let frame = CGRect(x: 0, y: 0, width: 100, height: 20);
                let view = TPCounterView(frame: frame);
                
                view.icon = FAKFontAwesome.starIcon(withSize: 9);
                view.title = "Title";
                view.value = "9,999";
                
                expect(view) == snapshot("TPCounterView");
                
            });
        });
    }
}
