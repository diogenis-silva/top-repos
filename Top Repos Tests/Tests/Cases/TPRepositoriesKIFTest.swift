//
//  TPRepositoriesKIFTest.swift
//  Top Repos Tests
//
//  Created by Diogenis Silva on 14/04/19.
//  Copyright © 2019 Diógenis Silva. All rights reserved.
//

import UIKit
import KIF

class TPRepositoriesKIFTest: KIFTestCase
{
    func testPullToRefresh()
    {
        tester().wait(forTimeInterval: 2);
        tester().pullToRefreshView(withAccessibilityLabel: "tableViewRepositories", pullDownDuration: .inAboutOneSecond);
    }
    
    func testPullToRefreshUntilAPIFail()
    {
        tester().wait(forTimeInterval: 1);
        
        for _ in 1...10
        {
            tester().pullToRefreshView(withAccessibilityLabel: "tableViewRepositories", pullDownDuration: .inAboutOneSecond);
            tester().wait(forTimeInterval: 0.5);
            
            if let _ = UIApplication.shared.keyWindow?.accessibilityElement(withLabel: "alertControllerOk")
            {
                tester().tapView(withAccessibilityLabel: "alertControllerOk");
                break;
            }
            
        }
    }
}
